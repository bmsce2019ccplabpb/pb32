#include<stdio.h>//deleting an given element
int main()
{
 int i,n,a[10],num,j;
 printf("Enter the number of elements: ");
 scanf("%d",&n);
 printf("Enter the elements: ");
 for(i=0;i<n;i++)
 {
  scanf("%d",&a[i]);
 }
 printf("Enter the element to delete: ");
 scanf("%d",&num);
 for(i=0;i<n;i++)
 {
  if(a[i]==num)
  {
   for(j=i;j<n-1;j++)
	a[j]=a[j+1];
   n=n-1;
  }
 }
 printf("The array after deleting an element: ");
 for(i=0;i<n;i++)
 {
  printf("%d  ",a[i]);
 }
 return 0;
}