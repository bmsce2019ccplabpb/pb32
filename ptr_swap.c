#include<stdio.h>
void swap(int *a,int *b)
{
 int temp;
 temp=*a;
 *a=*b;
 *b=temp;
}
int main()
{
 int n1,n2;
 printf("Enter two numbers: ");
 scanf("%d %d",&n1,&n2);
 printf("The value of n1 & n2 before swapping: %d , %d \n",n1,n2);
 swap(&n1,&n2);
 printf("The value of n1 & n2 after swapping: %d , %d \n",n1,n2);
 return 0;
}