#include <stdio.h>
int sumofdigits(int n);
int main()
{
 int n,sum;
 printf("Enter a number: ");
 scanf("%d",&n);
 sum=sumofdigits(n);
 printf("The sum of the digits are: %d",sum);
 return 0;
}
int sumofdigits(int n)
{
 int s=0,temp;
 while(n!=0)
 {
  temp=n%10;
  s=s+temp;
  n=n/10;
 }
 return (s);
}