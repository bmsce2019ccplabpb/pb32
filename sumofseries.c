#include<stdio.h>
float sumofseries(int n);
int main()
{
 int n;
 float sum=0.0;
 printf("Enter a number: ");
 scanf("%d",&n);
 sum=sumofseries(n);
 printf("The sum of the series is %f",sum);
 return 0;
}
float sumofseries(int n)
{
 int i=1;
 float s=0.0,temp;
 while(i!=n)
 {
  temp=(float)1/i;
  s=s+temp;
  i=i+1;
 }
 return s;
}