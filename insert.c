#include<stdio.h>//inserting in an unsorted array
int main()
{
 int n,i,num,p,a[10];
 printf("Enter the number of elements: ");
 scanf("%d",&n);
 printf("Enter the elements: ");
 for(i=0;i<n;i++)
 {
  scanf("%d",&a[i]);
 }
 printf("Enter the number to be inserted: ");
 scanf("%d",&num);
 printf("Enter the position for it to be inserted: ");
 scanf("%d",&p);
 for(i=n-1;i>=p;i--)
 {
  a[i+1]=a[i];
 }
 a[p]=num;
 n++;
 printf("The array after inserting: ");
 for(i=0;i<n;i++)
 {
  printf("%d ",a[i]);
 }
 return 0;
}