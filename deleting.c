#include<stdio.h>//deleting an element from a given position
int main()
{
 int i,n,a[10],pos;
 printf("Enter the number of elements: ");
 scanf("%d",&n);
 printf("Enter the elements: ");
 for(i=0;i<n;i++)
 {
  scanf("%d",&a[i]);
 }
 printf("Enter the position to delete: ");
 scanf("%d",&pos);
 for(i=pos;i<n-1;i++)
 {
  a[i]=a[i+1];
 }
 n=n-1;
 printf("The array after deleting an element: ");
 for(i=0;i<n;i++)
 {
  printf("%d  ",a[i]);
 }
 return 0;
}